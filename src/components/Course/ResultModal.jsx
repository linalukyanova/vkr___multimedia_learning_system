import React, { Component, Fragment } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class ResultModal extends Component {
   
    answer_info = (i) => {
        console.log(this.props.user_answers.length)
        if (this.props.time_up && i>=this.props.user_answers.length) return null;
        return (
        <>
            <h4>{this.props.test[i].question}</h4>
            
            <p>Ваш ответ: {this.props.user_answers[i].answers.map((ans, index)=>{
                console.log(ans)
                return(<>{ans} </>);
            })}
            </p>
            <p>Правильный ответ: {this.props.test[i].true_answer.map((true_ans, index)=>(
                <>{true_ans} </>
            ))}
            </p>
        </>
        );
    }

    render() {
      if (this.props.test !== null){  
        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.onClose}
                scroll='body'>
                <>
                     <DialogTitle>  
                        {(this.props.pass)?'Вы прошли тест':'Вы не прошли тест'}
                        
                     </DialogTitle>
                    <DialogContent>
                        <div>
                            Вы ответили на {this.props.right_ans} из {this.props.test.length}
                            {(this.props.user_answers.length >= this.props.test.length || this.props.time_up)?
                            this.props.test.map((info,index)=>{
                                return this.answer_info(index)
                            }):null}
                        </div>
                    </DialogContent>
                </>
            </Dialog>
        )
      } else {
          return null;
      }
    }
}

export default ResultModal;