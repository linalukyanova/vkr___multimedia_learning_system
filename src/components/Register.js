import React, { Component, Fragment } from 'react';
import TextInputGroup from './TextInputGroup';
import {withFirebase} from './Firebase/index'


class RegisterBase extends Component {
    constructor(props){
        super(props);
        this.state = {
            username : '',
            email : '',
            password : '',
            confirm_pass : '',
            errors : {}
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    validate = () =>  {
        const {username, email, password, confirm_pass} = this.state;
        let error_list = {};
        if (!(password === confirm_pass)) {
            error_list.password = "Password didn't match";
        } else if ((password === confirm_pass)) {
            
            delete error_list.password;
        }
        this.setState({
            errors : error_list
        })
        return (Object.keys(error_list).length === 0)?true:false;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {email, password} = this.state;
        if (this.validate()){
            this.props.firebase
            .doCreateUserWithEmailAndPassword(email,password).then(user=>{
                console.log(user);
            }).catch((err)=>{
                this.setState({errors:{password:err.message}})
            })
            console.log("Submit");
        }
    }

    render() {
        const {errors} = this.state;
        return (
           <Fragment>
               <form onSubmit={this.handleSubmit}>
                   <TextInputGroup 
                   label="Username"
                   id="username"
                   name="username"
                   value={this.state.username}
                   onChange={this.onChange}
                   />
                   <TextInputGroup 
                   label="e-mail"
                   id="email"
                   name="email"
                   value={this.state.email}
                   onChange={this.onChange}
                   />
                   <TextInputGroup
                   type="password"
                   label="Password"
                   id="password"
                   name="password"
                   value={this.state.password}
                   onChange={this.onChange}
                   />
                   <TextInputGroup
                   type="password"
                   label="Confirm Password"
                   id="confirm_pass"
                   name="confirm_pass"
                   value={this.state.confirm_pass}
                   onChange={this.onChange}
                   errorMessage={errors.password}
                   />
                   <button type="submit" value="Register"> Register </button>
               </form>
           </Fragment>
        );
    };
}

const Register = withFirebase(RegisterBase);

export default Register;