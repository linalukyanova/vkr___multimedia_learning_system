import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl';

//Landing//
import MainHeader from "./components/Landing/header";
import Main from "./components/Landing/main";
import Footer from "./components/Landing/footer";

import SignUpPage from '../src/components/Login/SignUp';
import SignInPage from '../src/components/Login/SignIn';
import Profile from '../src/components/Profile/Profile';
//import PasswordForgetPage from '../src/components/PasswordForget';
import HomePage from './components/Home/Home';
import Course from './components/Course/Course';
import Lesson from './components/Lesson/Lesson'
import NotFound from './components/PageNotFound';
import CourseTest from './components/Course/CourseTest'
import UploadCourse from './components/Upload'
//import ProfilePage from '../src/components/Profile';
//import AdminPage from '../Admin';
//import AdminPage from '../About';
//import AdminPage from '../Download';
//......................Страницы для обучения/тестирования........................ 

import * as ROUTES from '../src/constants/routes';

import './App.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('token')!== null
      ? <Component {...props} />
      : <Redirect to={{
          pathname: ROUTES.SIGN_IN,
          state: { from: props.location }
        }} />
  )} />
)

function App() {
    return (
      
      <Router>
        <Fragment>
        {/* <div className="container">
        <Header />
        <Main />
        <Footer />
      </div>  */}

          <Route exact path={ROUTES.LANDING} component={MainHeader} />
          <Route exact path={ROUTES.LANDING} component={Main} />
          <Route exact path={ROUTES.LANDING} component={Footer} />
          <Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
          <Route exact path={ROUTES.SIGN_IN} component={SignInPage} />
          <PrivateRoute exact path={ROUTES.HOME} component={HomePage} />
          <PrivateRoute exact path={ROUTES.PROFILE} component={Profile} />
          <PrivateRoute path={ROUTES.COURSE} exact component={Course}/>
          <PrivateRoute path={ROUTES.LESSON} exact component={Lesson}/>
          <PrivateRoute exact path={ROUTES.COURSE_TEST} component={CourseTest}/>
          <Route exact path={ROUTES.UPLOAD_COURSE} component={UploadCourse}/>
          {/* <Route path="*" component={NotFound} /> */}
          
        </Fragment>
      </Router>
    );
}

export default App;
