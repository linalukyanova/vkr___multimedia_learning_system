import React, { Component, Fragment } from 'react';
import {Grid} from '@material-ui/core'
import {Link} from 'react-router-dom'
import Navbar from '../Navbar';
import styled from 'styled-components';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';


class SingleType extends Component {
    // question, обработка кнопки 

    constructor(props){
        super(props);
        this.state = {
            answered: false,
            is_right: false,
            disabled_end_button: false
        }
    }

    checkAnswer = (e) => {
        if (this.state.answered) return;
        let answer = e.target.id
        let is_right = false;
        console.log(this.props.question.true_answer[0])
        if (answer === this.props.question.true_answer[0]) {
            console.log("You're right")
            is_right = true
        } else {
            console.log("Nope")
        }
        this.setState({answered:true, is_right:is_right})
    }

    render() {
        let {question, answers, true_answer} = this.props.question;
        return (<div>
            <div style={{display:'flex', flexDirection:'column',justifyContent:'center', alignItems:'center'}}>
                <h2>Вопрос №{this.props.num}/{this.props.count}</h2>
                <p className="test-question">{question}</p> 
                <Grid container style={{maxWidth:'50%', justifyContent:'center'}}spacing={3} >
                    {answers.map((answer)=>(
                        <Grid item xs={6} key={answer} id={answer} >
                            <div className="test-answers" style={{width:'100%', textAlign:"center", backgroundColor:'rgba(240,240,240,0.7)'}} id={answer} onClick={this.checkAnswer}>{answer}</div>
                        </Grid>))}
                    
                </Grid>
                {(this.state.answered)
                ?   <div>
                        <h4>{(this.state.is_right)?'Вы ответили верно':'Вы ответили неверно'}</h4>
                        {(!this.state.is_right)?<h4>Правильный ответ: {true_answer[0]}</h4>:null }
                        {this.props.num < this.props.count?
                        <button type="button" onClick={()=>{this.setState({answered:false, is_right:false}); this.props.nextQuestion(this.state.is_right)}}>Далее</button>
                        : <button type="button" disabled={this.state.disabled_end_button} onClick={()=>{this.setState({disabled_end_button:true}); this.props.nextQuestion(this.state.is_right)}}>Завершить</button>}
                    </div>
                :null}
            </div>
        </div>)
    }
}

export default SingleType