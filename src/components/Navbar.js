import React, {Component, Button} from 'react';
import {withFirebase} from './Firebase/index'
import { withRouter } from 'react-router-dom';
import {Menu, MenuItem} from 'react-mdl';
import { compose } from 'recompose';
import * as ROUTES from './../constants/routes'

class AppBar extends Component  {
    constructor(props){
        super(props);
    }
    signout = () => {
        localStorage.removeItem('token');
        this.props.firebase.doSignOut().then((res)=>{
            this.props.history.push(ROUTES.LANDING)
        })
    }

    render() {
        return (
            <div className="navbar" style={{left:this.props.marginLeft, width: ((window.screen.width-50-this.props.marginLeft)*100/window.screen.width) +'%'}}>
                <div className="left-navbar">
                    <p>{this.props.title}</p>
                </div>
                <div className="nav-links">
                    <div id="test">Кликни!</div>
                    <Menu target="test">
                        <MenuItem onClick={()=>{console.log("И тебе привет")}}>Привет</MenuItem>
                        <MenuItem>Я</MenuItem>
                        <MenuItem disabled>Возможно</MenuItem>
                        <MenuItem>Рабочий</MenuItem>
                        <MenuItem>Пример</MenuItem>
                    </Menu>
                    <div className="nav-signout" onClick={this.signout}></div>
                </div>
            </div>
          );
    } 
  }

export default compose(withFirebase, withRouter)(AppBar);