import React, { Component } from 'react';
import Nav from "./nav";
import './landing.css'

class Header extends Component {
  render() {
    return (
     
      <header className="header-landing">
      <Nav/>
        <div className="head">
            <h1>Знания,< br/> доступные каждому!</h1>
            <div>
              <p>Описание моего удивительного мультимедийного обучающего комплекса, которое заставит пользователей им воспользоваться, которое еще нужно придумать!</p>
              <div><a className="contact" href="/signup">Начать учиться сейчас!</a></div>
            </div>
        </div>
       
      </header>
      
    );
  }
}

export default Header;
