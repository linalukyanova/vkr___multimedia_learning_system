import React from "react";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

export default function SideBarBack(props) {
    return (
    <div className="back-arrow" onClick={props.redirectBack}>
    <div className="back-arrow-img"><ArrowBackIcon fontSize="large"/></div>
    {(props.expanded)?<p style={{fontSize:"large", paddingLeft:"10px", paddingTop:"5px"}}>{props.title}</p>:null}  
    </div>
  )
}