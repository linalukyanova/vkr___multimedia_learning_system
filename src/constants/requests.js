const SERVER_HOST = 'https://tranquil-fortress-68060.herokuapp.com/api'
// Передаем в QUERY: user_id
export const GET_TOKEN = SERVER_HOST + '/firebase/getUserToken';
// Во всем ниже передаем токен через header 'user-key'
// Ничего не передаем в query
export const GET_COURSES = SERVER_HOST + '/posts/getCourses';
// Ничего не передаем в query
export const GET_ACTIVE_COURSES = SERVER_HOST + '/posts/getActiveCourses';
// Передаем в QUERY: course
export const GET_LESSON_LIST = SERVER_HOST + '/posts/getLessonList';
// Передаем в QUERY: course, lesson_no
export const GET_LESSON_TEST = SERVER_HOST + '/posts/getLessonTest';
// Передаем в QUERY: course
export const GET_COURSE_TEST = SERVER_HOST + '/posts/final_test';
// Передаем в QUERY: course
export const GET_COURSE_RESULTS = SERVER_HOST + '/posts/getCourseResults';
// Передаем в QUERY: image
export const GET_IMAGE = SERVER_HOST + '/posts/getImage';
// Передаем в QUERY: video
export const GET_VIDEO = SERVER_HOST + '/posts/getVideo';
// Передаем в QUERY: course, lesson_no
export const PUT_LESSON_TEST_RESULT = SERVER_HOST + '/puts/putLessonTestResult'; 
// Передаем в QUERY: course, lesson_no
export const PUT_LESSON_RESULT = SERVER_HOST + '/puts/putLessonTestResult'; 
// Передаем в QUERY: course
export const PUT_FINAL_RESULT = SERVER_HOST + '/puts/putFinalResult';
// Передаем в QUERY: course
export const PUT_SIGN_ON_COURSE = SERVER_HOST + '/posts/signOnCourse';