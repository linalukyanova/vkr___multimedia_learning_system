import React, {Component} from 'react';
import {Input} from '@material-ui/core'


class InputType extends Component {
    constructor(props){
        super(props)
        this.state = {
            answer : '',
            answered: false,
            is_right: false,
            disabled_end_button: false
        }
    }

    changeValue = (index) => {
        let temp = this.state.answers
        temp[index] = !temp[index]
        this.setState({answers:temp})
    }

    checkAnswer = () => {
        if (this.state.answered) return;
        let is_right = false;
        if (this.state.answer.toLowerCase()===this.props.question.true_answer[0].toLowerCase()){
            is_right = true;
        }
        this.props.nextQuestion({answers: [this.state.answer], is_right: is_right});

    }

    render(){
        let {question, true_answer} = this.props.question;
        return (<div>
            <div style={{display:'flex', flexDirection:'column',justifyContent:'center', alignItems:'center'}}>
                <h2>Вопрос №{this.props.num}/{this.props.count}</h2>
                <p className="test-question">{question}</p> 
                <Input value={this.state.answer} onChange={(e)=>this.setState({answer:e.target.value})}/>
                <button type="button" onClick={this.checkAnswer} >Ответить</button>
            </div>
        </div>)
    }
}

export default InputType;