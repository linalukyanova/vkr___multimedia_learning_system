import React from 'react';
import { Link } from 'react-router-dom';
//import PageNotFound from '../assets/img/404.jpg';

class NotFoundPage extends React.Component{
    render(){
        return <div>
        <h1>404- Page NotFound</h1>
            <p style={{textAlign:"center"}}>
              <Link to="/">Go to Home </Link>
            </p>
          </div>;
    }
}
export default NotFoundPage;