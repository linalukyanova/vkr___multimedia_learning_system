import React, { Component, Fragment } from 'react';
import {Grid} from '@material-ui/core'
import axios from 'axios';
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import Navbar from '../Abar';
import EditIcon from '@material-ui/icons/Edit';
import AccountSettingsModal from './Account'
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import Tooltip from '@material-ui/core/Tooltip';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import * as REQUESTS from '../../constants/requests'


class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            started: '...',
            completed: "...",
            courses_list: [],
            open: false, 
            all_courses: null
        }   
    }
    async get_active_courses() {
        return axios.get('https://tranquil-fortress-68060.herokuapp.com/api/posts/getActiveCourses', {
            headers: {
                'user-key': localStorage.getItem('token')
            }
        })
    }

    get_all_courses(){
      return axios.get(REQUESTS.GET_COURSES,{
        headers: {
          'user-key': localStorage.getItem('token'),
        }
      })
    }

    get_percent(progress_array){
      let ret_val = 0;
      progress_array.forEach(element => {
        if (element === true) ret_val++;
      })
      return Math.round(ret_val/progress_array.length*100);
    }

    componentDidMount() {
        this._asyncRequest = this.get_active_courses().then((res) => {
            let ret_val=[];
            let completed = 0;
            console.log("ya tuta");
            console.log(res);
            res.data.forEach(element => {
                console.log(element)
                let percent = this.get_percent(element.user_progress);
                if (percent >= 100) completed+=1;
                ret_val.push({
                  title:element.title,
                  percent: percent
                })
            });
            this.setState({
              started: res.data.length,
              courses_list:ret_val,
              completed:completed
            });
        this.get_all_courses().then(res=>{
            console.log("Получил курсы: ", res.data)
            this.setState({all_courses:res.data})
        })
      })
    }
    responsive = {
      0: {items:6}
    }

    stagePadding = {
      paddingLeft: 10,
      paddingRight: 10,
    }
    render(){
      let staff = null;
      var items = [];
      var cards = []
      if (this.state.courses_list.length !== 0) {
        for (var i in this.state.courses_list){
            items.push(<CoursesProgress title={this.state.courses_list[i].title} key={i}>
              <CircularProgressbar value={this.state.courses_list[i].percent} text={this.state.courses_list[i].percent+'%'}/>
            </CoursesProgress>);
        }
      }
      if (this.state.all_courses !== null) {
        for (var i in this.state.all_courses){
          cards.push(<CourseCard course={this.state.all_courses[i]}/>)
        }
      }
        return (
            <div style={{width: '100%', margin: 'auto'}}>
              <Navbar title="Профиль" marginLeft={0} />
        <Grid container spacing={7}  className="landing-grid" style={{'margin-top':'64px', paddingTop: '20px'}}>
          <Grid item xs={3}>
            <div className="user-info" style={{paddingBottom:'15px'}}>
              <img
                src="https://cdn2.iconfinder.com/data/icons/avatar-2/512/Fred_man-512.png"
                alt="avatar"
                className="avatar-img"
                />

              <div className="banner-text">
                
                <h2>{localStorage.getItem('username')}  </h2>
                <EditIcon onClick={()=>{this.setState({open:true})}} style={{color:"white", position: "relative", left:"120px", top:"-70px", fontSize:"1.7em", paddingLeft:"12px"}} />

              <hr style={{marginTop:"-25px"}}/>
              <p>Курсов завершено: {this.state.completed}</p>
              <p>Курсов начато: {this.state.started}</p>
              
              </div>
            </div>
          </Grid>
          <Grid container direction="column" xs={8}>
          <Grid item xs>
            <div className="active-courses">
              <h2>Активные курсы</h2> 
              <hr/>
              <Carousel slidesPerScroll={2} slidesPerPage={4} arrows slides={items}></Carousel>
            </div>
          </Grid>
          <Grid item xs>
            <div className="active-courses" onClick={()=>this.props.history.push("/home")}>
              <h3>Попробуйте наши курсы!</h3>
              <Carousel autoPlay={2000} animationSpeed={1000}  infinite slidesPerScroll={2} slidesPerPage={6} slides={cards}/>
            </div> 
          </Grid>
          </Grid>
        </Grid>
        <AccountSettingsModal open={this.state.open} handleClose={()=>{this.setState({open:false})}}/>
      </div>
        )
    }
}

function CourseCard(props) {
  return <Card shadow={5} style={{maxWidth: 150, marginTop: '15px', marginBottom:"15px"}}>
  <CardMedia
    image={REQUESTS.GET_IMAGE+'?image='+props.course['codename']}
    title="Contemplative Reptile"
    style={{color: '#fff', height: '140px'}}
  />
  <CardContent>
    <Typography gutterBottom variant="p" component="h4">
      {props.course['title']}
    </Typography>
  </CardContent>
  </Card>
}

function CoursesProgress(props) {
    return (
      <Tooltip title={props.title} placement="right">
      <div className="progress-block">
        <div>{props.children}</div>
        <h3>{props.title}</h3>
      </div>
      </Tooltip>
    );
}

export default Profile;
