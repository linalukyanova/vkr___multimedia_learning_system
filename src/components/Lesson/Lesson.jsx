import React, { Component, Fragment } from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton, List, ListItem, ListItemContent, ListItemAction,Icon } from 'react-mdl';
import {Link, withRouter} from 'react-router-dom'
import Navbar from '../Abar';
import styled from 'styled-components';
import SideNav, { Toggle, Nav, NavHeader, NavTitle, NavSubTitle, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';
import { Player } from 'video-react';
import LessonTest from './LessonTest'
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';

import { withTheme } from '@material-ui/core';
import SideBarBack from '../SideBarBack'

const navbarCollapsed = 64;
const navbarExpanded = 250;
const initialSelect = 0;
class Lesson extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      categories:null,
      has_test:null,
      expanded: false,
      selected: initialSelect,
      selected_course: null,
      current_section: 0,
      lesson: null,
      types: null,
    };
  }


  getLessonList = () => {
    let codename = this.props.match.params.courseName;
    console.log(codename)
    return axios.get(REQUESTS.GET_LESSON_LIST, {
      params: {
        course: codename
      },
      headers: {
        'user-key': localStorage.getItem('token')
      }
    })
  }

  wrapHtml = (html) => {
    return {__html:html}
  }


  putLessonResult = () => {
    var codename = this.props.match.params.courseName;
    var lesson_no = this.props.match.params.id
    return axios.put(REQUESTS.PUT_LESSON_RESULT,{}, {
        headers: {
            'user-key': localStorage.getItem('token')
        }, 
        params: {
            course: codename,
            lesson_no: lesson_no,
        }
    }).then((res)=>{
        this.handleRedirect();
    })
  }

  handleRedirect = () => {
    let curr_url = window.location.pathname;
    curr_url = curr_url.split('/');
    curr_url = curr_url.slice(0,curr_url.length-2);
    curr_url = curr_url.join('/');
    this.props.history.push(curr_url)
  }

  showContent = (lesson, current_section) => {
    let {selected} = this.state;
    console.log(selected)
    if (selected === 'test')
      return (<LessonTest courseName={this.props.match.params.courseName} lesson_no={this.props.match.params.id}/>);
    else 
    return (
      <>
      <h1 style={{textAlign:'center'}}>{lesson.section_titles[current_section]}</h1>
      {(lesson.section_types[current_section]==='text')?
       <div style={{width:'85vw', margin:'auto', padding: 20, backgroundColor: '#fff', fontSize: '1.3em', borderRadius: '7px'}} dangerouslySetInnerHTML={this.wrapHtml(lesson.sections[current_section])}></div>:
       <div className="player-wrapper" style={{margin:'auto', width:'70vw', height:'80vh'}}>
       <Player>
            <source src={lesson.sections[current_section]}/>
       </Player>
      </div>}
      {(this.state.has_test !== true && current_section==lesson.sections.length-1)?<div className="end-lesson"><button className="end-lesson" onClick={this.putLessonResult}>Завершить лекцию</button></div>:null }
      </>
      
    
    )
  }

  redirectBack = () => {
    this.props.history.push("/course/"+this.props.match.params.courseName);
  }
  componentDidMount() {
    let lesson_no = this.props.match.params.id;
    this._asyncRequest = this.getLessonList().then((res) => {
        console.log(res)
        this.setState({
          lesson:res.data[lesson_no],
          categories:res.data[lesson_no].section_titles,
          types: res.data[lesson_no].section_types,
          has_test:res.data[lesson_no].include_test
        });
        
  })
}
  render() {
    let {lesson, categories, current_section, has_test, types} = this.state
    let codename = this.props.match.params.courseName;
    return(
      <div>
        <Navbar title={'Лекция №'+Number(Number(this.props.match.params.id)+1)}marginLeft={(this.state.expanded)?navbarExpanded:navbarCollapsed}/>
        <CourseSideNav 
          categories={categories!==null?categories:null}
          types={types!==null?types:null}
          has_test={has_test!==null?has_test:null}
          onSelect={(selected)=>{this.setState({selected:selected, current_section:selected}); console.log(selected)}}
          onToggle={(expanded)=>{this.setState({expanded:expanded})}} 
          categories={(this.state.categories === null)?null:this.state.categories} 
          expanded={this.state.expanded}
          redirectBack={this.redirectBack}
          />
        <div className="lesson-content"style={{marginLeft: (this.state.expanded)?navbarExpanded:navbarCollapsed}}>
          {lesson!==null?
            <div>
              {this.showContent(lesson, current_section)}
              
            </div>:null
          }
        </div>
      </div>
    )
  }
}


class CourseSideNav extends Component {


    render() {
      let {categories, has_test,types} = this.props;
      console.log('Titles '+categories);
      console.log('has test? '+has_test)
      console.log('types: ', types)
      return (

        <SideNav style={{display:'block', width:this.props.expanded?navbarExpanded:navbarCollapsed}}
          onSelect={this.props.onSelect}
          onToggle={this.props.onToggle}
        >
          <SideNav.Toggle/>
        
          <SideNav.Nav defaultSelected={initialSelect}>
            {
              (categories !== null)?
              categories.map((item, index)=>(
              <NavItem eventKey={categories.indexOf(item)} key={item}>
                <NavIcon>
                {types[index] === 'video'?<VideoLibraryIcon/>: <MenuBookIcon/>}
                </NavIcon>
                <NavText>
                {item}
                </NavText>

              </NavItem>
            )):null}
            {
              (has_test!==null && has_test === true)
                ? 
                <NavItem eventKey={"test"} >
                  <NavIcon >
                    <QuestionAnswerIcon/>
                  </NavIcon>
                  <NavText>
                    Тест
                  </NavText>
                </NavItem>
                :null
              
            }
            <SideBarBack expanded={this.props.expanded} redirectBack={this.props.redirectBack} title={"К списку лекций"}/>
          </SideNav.Nav>
        </SideNav>
      )
    }

}


export default withRouter(Lesson);
