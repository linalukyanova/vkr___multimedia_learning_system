import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

class MultiType extends Component {
    constructor(props){
        super(props)
        this.state = {
            answers : [false,false,false,false],
            answered: false,
            is_right: false,
            disabled_end_button: false,
        }
    }

    checkAnswers = () => {
        if (this.state.answered) return;
        let {answers, true_answer} = this.props.question;
        let right_count = 0;
        let is_right = false;
        let user_answers = [];
        console.log(this.state.answers)
        for (var i = 0;i<this.state.answers.length;i++) {
            let result = this.state.answers[i]
            if (result === true) {
                if (true_answer.indexOf(answers[i]) != -1) right_count++
                else {right_count=0; break;}
            }
        }
        for (var i = 0; i< this.state.answers.length;i++) {
            let result = this.state.answers[i]
            if (result === true) {
                user_answers.push(answers[i]);
            }
        }
        console.log('len: '+true_answer.length)
        console.log('r_count: '+right_count)
        if (true_answer.length === right_count){
            is_right = true
        } 
        this.setState({answers:[false,false,false,false]})
        this.props.nextQuestion({answers:user_answers,is_right:is_right});
    }

    changeValue = (index) => {
        let temp = this.state.answers
        temp[index] = !temp[index]
        this.setState({answers:temp})
    }

    render(){
        let {question, answers, true_answer} = this.props.question;
        return (<div>
            <div style={{display:'flex', flexDirection:'column',justifyContent:'center', alignItems:'center'}}>
                <h2>Вопрос №{this.props.num}/{this.props.count}</h2>
                <p className="test-question">{question}</p> 
                <FormControl component='fieldset'>
                    <FormGroup>
                        {answers.map((answer,i)=>
                            <FormControlLabel 
                            control={<Checkbox checked={this.state.answers[i]} onChange={()=>this.changeValue(i)}/>}
                            label={answer}/>
                        )}
                        
                    </FormGroup>
                </FormControl>
                <button type="button" onClick={this.checkAnswers}>Ответить</button>
            </div>
        </div>)
    }
}

export default MultiType;