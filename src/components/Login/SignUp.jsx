import React, { Component, Fragment } from 'react';
import TextInputGroup from '../TextInputGroup';
import {withFirebase} from '../Firebase/index'
//import '../../styles/signin.css'
import * as ROUTES from '../../constants/routes'

class RegisterBase extends Component {
    constructor(props){
        super(props);
        this.state = {
            username : '',
            email : '',
            password : '',
            confirm_pass : '',
            errors : {}
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    validate = () =>  {
        const {username, email, password, confirm_pass} = this.state;
        let error_list = {};
        if (!(password === confirm_pass)) {
            error_list.password = "Password didn't match";
        } else if ((password === confirm_pass)) {
            
            delete error_list.password;
        }
        this.setState({
            errors : error_list
        })
        return (Object.keys(error_list).length === 0)?true:false;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const {email, password} = this.state;
        if (this.validate()){
            this.props.firebase
            .doCreateUserWithEmailAndPassword(email,password).then(res=>{
                res.user.updateProfile({
                    displayName: this.state.username
                });
                this.props.history.push(ROUTES.SIGN_IN);
            }).catch((err)=>{
                this.setState({errors:{password:err.message}})
            })
            console.log("Submit");
        }
    }

    render() {
        const {errors} = this.state;
        console.log(localStorage.getItem('token'));
        if (localStorage.getItem('token') !== null){
            
            this.props.history.push(ROUTES.PROFILE);
        }
        return (
            <Fragment>
                <div className="wrapper">
                    <div className={'authContainer'}>
                        <div className={'leftBox'}>
                            <div className={'bgGreen'}/>
                            <div className={'imageRegister'}/>
                            <div className={'imageText bold'}>Начни учиться прямо сейчас!</div>
                        </div>
                        <div className={'rightBox'}>
                            <form onSubmit={this.handleSubmit} className={'form'}>
                                <div className={'titleAuth'}>Регистрация</div>
                                <div className={'inputSBox'}>
                                <TextInputGroup 
                                    id="username"
                                    name="username"
                                    className={'inputS'} 
                                    placeholder={'Username'}
                                    value={this.state.username}
                                    onChange={this.onChange}
                                />
                                </div>
                                <div className={'inputSBox'}>
                                    <TextInputGroup 
                                    id="email"
                                    name="email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    className={'inputS'} 
                                    placeholder={'Email'}
                                    />
                                </div>
                                <div className={'inputSBox'}>
                                    <TextInputGroup
                                        className={'inputS'}
                                        type="password"
                                        id="password"
                                        name="password"
                                        placeholder={'Password'}
                                        value={this.state.password}
                                        onChange={this.onChange}
                                    />
                                </div>    
                                <div className={'inputSBox'}>
                                    <TextInputGroup
                                        type="password"
                                        id="confirm_pass"
                                        name="confirm_pass"
                                        className={'inputS'} 
                                        placeholder={'Confirm Password'}
                                        value={this.state.confirm_pass}
                                        onChange={this.onChange}
                                        errorMessage={errors.password}
                                    />
                                </div>    
                                <button type="submit" value="Register" className={'btnAuth register'}> Зарегистрироваться </button>
                                <a href='/signin'>Уже зарегистрированы? Войти.</a>
                            </form>
                            
                        </div>
                    </div> 
                </div>   
        </Fragment>
        );
    };
}

const Register = withFirebase(RegisterBase);

export default Register;