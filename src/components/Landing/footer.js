import React, { Component } from 'react';
import './landing.css'

class Footer extends Component {
  render() {
    return (
     
      <footer>
          <h3>Design by linalukyanova</h3>
          <p>Support <br/> tabakova.g@yandex.ru</p>
          <ul>
            <li><a href="#"><i className="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i className="fab fa-twitter"></i></a></li>
            <li><a href="#"><i className="fab fa-instagram"></i></a></li>
          </ul>
      </footer>
      
    );
  }
}

export default Footer;
