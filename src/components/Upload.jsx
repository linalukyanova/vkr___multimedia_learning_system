import React, {Component} from 'react';
import {useDropzone} from 'react-dropzone';

function UploadCourse() {
    const {getRootProps, getInputProps, acceptedFiles} = useDropzone({noDrag: true});
    const files = acceptedFiles.map(file => <li key={file.path}>{file.path}</li>);
  
    return (
      <section className="container">
        <div {...getRootProps({className: 'dropzone'})}>
          <input {...getInputProps()} />
          <p>Dropzone with no drag events</p>
          <em>(Drag 'n' drop is disabled)</em>
        </div>
        <aside>
          <h4>Files</h4>
          <ul>{files}</ul>
        </aside>
      </section>
    );
}

export default UploadCourse;
