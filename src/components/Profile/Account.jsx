import React, {Component} from 'react'
import {withFirebase} from '../Firebase/index';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


class AccountSettingsModal extends Component {
    
    constructor(props){
        super(props)
        this.state = {
            user: null,
            provider: null,
            username_input: localStorage.getItem('username'),
            email_input: "",
            new_pass_input:"",
            confirm_pass_input:"",
            enabled: {
                username_input: false,
                email_input: false,
                new_pass_input:false,
                confirm_pass_input:false,
            }
        }
    }

    componentDidMount() {
        var {firebase} = this.props;
        firebase.auth.onAuthStateChanged((user)=>{
            if (user!=null)  this.setState({user: user, provider:user.providerData[0].providerId})
        })
    }

    handleUpdateUsername = () => {
        this.props.firebase.doUsernameUpdate(this.state.username_input).then(()=>{
            console.log("Success on updating username");
            localStorage.setItem('username', this.state.username_input);
        })
    }

    handleUpdatePass = () => {
        if (this.state.new_pass_input == this.state.confirm_pass_input)
            this.props.firebase.doPasswordUpdate(this.state.new_pass_input).then(()=>{
                console.log("Success on updating password");
            });
    }

    handleUpdateEmail = () => {
        this.props.firebase.doEmailUpdate(this.state.email_input).then(()=>{
            console.log("Success on updating email");
        })
    }

    onChange = (e) => {
        var key = e.target.name;
        var {enabled} = this.state
        enabled[key]=true;
        this.setState({
            [e.target.name]: e.target.value,
            enabled
        });
        console.log(this.state.enabled)
    };

    render(){   
        var {open, handleClose} = this.props; 
        return (
            <Dialog
            open={open}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            >
        <DialogTitle id="alert-dialog-slide-title">{"Настройки аккаунта"}</DialogTitle>
        <DialogContent>
            <div style={{display:"flex", flexDirection:"row", marginBottom:"15px"}}>
                <div style={{display:"flex", flexDirection:"column", marginRight:"10px"}}>
                    <Input  placeholder="Введите имя пользователя" value={this.state.username_input} name="username_input" onChange={this.onChange} style={{fontSize:"large", marginBottom:"10px"}}></Input>
                    <Button disabled={!this.state.enabled.username_input} variant="contained" color="primary"onClick={this.handleUpdateUsername}>Обновить имя пользователя</Button>
                </div>
                {(this.state.provider === "password")?
                <div style={{display:"flex", flexDirection:"column"}}>
                    <Input placeholder="Email" value={this.state.email_input} name="email_input" onChange={this.onChange} style={{fontSize:"large", marginBottom:"10px"}}></Input>
                    <Button disabled={!this.state.enabled.email_input} variant="contained" color="primary" onClick={this.handleUpdateEmail}>Обновить email</Button>
                </div>:null}
            </div>
            {(this.state.provider === "password")?
            <div style={{display:"flex", flexDirection:"column"}}>
                <div style={{display:"flex", flexDirection:"row", paddingTop:"15px", marginBottom:"15px", borderTop:"2px dotted black"}}>
                    <Input placeholder="Новый пароль" type="password" value={this.state.new_pass_input} name="new_pass_input" onChange={this.onChange} style={{fontSize:"large", marginRight:"10px"}}></Input>
                    <Input placeholder="Подтвердите пароль" type="password" value={this.state.confirm_pass_input} name="confirm_pass_input" onChange={this.onChange} style={{fontSize:"large", marginRight:"10px"}}></Input>
                </div>
                <Button variant="contained" color="primary" disabled={!this.state.enabled.confirm_pass_input || !this.state.enabled.new_pass_input} onClick={this.handleUpdatePass}>Обновить пароль</Button>
            </div>:null}
            

        </DialogContent>
      </Dialog>)
    }
}

export default withFirebase(AccountSettingsModal)