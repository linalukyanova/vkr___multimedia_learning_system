import app from 'firebase/app';
import 'firebase/auth';
var config = {
    apiKey: "AIzaSyBbmMPVoy30niogyGj-kJsHXloL5kQAKc4",
    authDomain: "kurswork2019-1570564534821.firebaseapp.com",
    databaseURL: "https://kurswork2019-1570564534821.firebaseio.com",
    projectId: "kurswork2019-1570564534821",
    storageBucket: "kurswork2019-1570564534821.appspot.com",
    messagingSenderId: "393471403703",
    appId: "1:393471403703:web:7427ebaa5e8347dfafcf32",
    measurementId: "G-3L0BEFJ90P"
}

// const config = {
//     apiKey: process.env.REACT_APP_API_KEY,
//     authDomain: process.env.REACT_APP_AUTH_DOMAIN,
//     databaseURL: process.env.REACT_APP_DATABASE_URL,
//     projectId: process.env.REACT_APP_PROJECT_ID,
//     storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
//     messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
//     appId: process.env.REACT_APP_APP_ID,
//     measurementId:process.env.REACT_APP_MEASUREMENT_ID,
//   };

class Firebase {
    constructor(){
        app.initializeApp(config);

        this.auth = app.auth();

        this.googleProvider = new app.auth.GoogleAuthProvider();
        this.facebookProvider = new app.auth.FacebookAuthProvider();
    }
    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignInWithGoogle = () =>
        this.auth.signInWithPopup(this.googleProvider);
    
    doSignInWithFacebook = () =>
        this.auth.signInWithPopup(this.facebookProvider);
        
    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
    
    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);

    doEmailUpdate = email => 
        this.auth.currentUser.updateEmail(email);

    doUsernameUpdate = username => 
        this.auth.currentUser.updateProfile({displayName:username});
}

export default Firebase;