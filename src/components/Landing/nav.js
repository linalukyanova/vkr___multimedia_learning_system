import React, { Component } from 'react';
import './landing.css'

class Nav extends Component {
  render() {
    return (
      <nav style={{width: '100vw'}}>
          <ul>
               <li className="logo">Education<span>App</span></li>
          </ul>
          <ul>
              <li><a href="#">About</a></li>
              <li><a href="#">Download</a></li>
              <li><a href="#">Author</a></li>
              <li><a href="#">Contact</a></li>
          </ul>
      </nav>
    );
  }
}

export default Nav;
