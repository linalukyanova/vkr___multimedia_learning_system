import React, { Component, Fragment } from 'react';
import {Link, withRouter} from 'react-router-dom';
import Navbar from '../Navbar';
import styled from 'styled-components';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';
import SingleType from './SingleTypeTest'
import MultiType from './MultiTypeTest'
import InputType from './InputTypeTest';
import LessonTestResultModal from './LessonTestResultModal'

class LessonTest extends Component {

    constructor(props){
        super(props);
        this.state = {
            test: null,
            current_question: 0,
            right_answers: 0,
            show_modal: false
        }
    }

    getLessonTest = () => {
        let codename = this.props.courseName;
        let lesson_no = Number(this.props.lesson_no)+1;
        console.log(codename)
        console.log(lesson_no)
        return axios.get(REQUESTS.GET_LESSON_TEST+'?course='+codename+"&lesson_no="+lesson_no, {
          headers: {
            'user-key': localStorage.getItem('token')
          }
        })
      }

    componentDidMount() {
        this._asyncRequest = this.getLessonTest().then((res) => {
            console.log(res)
            this.setState({
              test:res.data
            });
            
      })
    }

    putLessonTestResult() {
        return axios.put(REQUESTS.PUT_LESSON_TEST_RESULT,{}, {
            headers: {
                'user-key': localStorage.getItem('token')
            }, 
            params: {
                course: this.props.courseName,
                lesson_no: this.props.lesson_no,
            }
        })
    }

    showQuestion = (type) => {
        let {test, current_question} = this.state
        if (type == 'single')
            return <SingleType question={test[current_question]} count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
        else if (type == 'multi')
            return <MultiType question={test[current_question]}  count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
        else if (type == 'input')
            return <InputType question={test[current_question]}  count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
    }

    nextQuestion = (result) => {
        let {current_question} = this.state;
        let new_val = (result)?++this.state.right_answers:this.state.right_answers
        let new_cur_val = current_question
        let show_modal = false;
        if (current_question < this.state.test.length-1) new_cur_val++;
        else show_modal = true;
        this.setState({current_question:new_cur_val, right_answers:new_val, show_modal: show_modal})
        console.log('right ans: '+ new_val)
    }

    handleClose = () => {
        this.setState({show_modal:false})
        this.handleRedirect();
    }

    handleRedirect = () => {
        var {right_answers,test} = this.state;
        var question_num = test.length;

        console.log("right: ", right_answers, " question num: ", question_num);
        let curr_url = window.location.pathname;
        curr_url = curr_url.split('/');
        curr_url = curr_url.slice(0,curr_url.length-2);
        curr_url = curr_url.join('/');
        if (right_answers/question_num*100 >= 60) this.putLessonTestResult();
        this.props.history.push(curr_url)
    }

    render() {
        let {test, current_question} = this.state
        return (
        <>
            {test !== null ? this.showQuestion(test[current_question].type) : null} 
            {test !== null ? 
            <LessonTestResultModal 
                handleClose={this.handleClose}
                handleRedirect={this.handleRedirect}
                show={this.state.show_modal}
                right_ans={this.state.right_answers}
                question_num={test.length}
            /> : null }
        </>)
    }
}

export default withRouter(LessonTest)