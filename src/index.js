import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import 'react-mdl/extra/material.js';

import Firebase, {FirebaseContext} from './components/Firebase/index';

import App from './App';

ReactDOM.render(
  <FirebaseContext.Provider value={new Firebase()}>
    <App />
  </FirebaseContext.Provider>,
  document.getElementById('root')
);