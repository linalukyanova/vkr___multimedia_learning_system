import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import StarIcon from '@material-ui/icons/Star';
import CheckIcon from '@material-ui/icons/Check';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ListSubheader from '@material-ui/core/ListSubheader';
import AssignmentLateIcon from '@material-ui/icons/AssignmentLate';
import Navbar from '../Abar';
import styled from 'styled-components';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';
import { Modal } from 'react-responsive-modal';
import SideBarBack from '../SideBarBack';
import * as ROUTES from '../../constants/routes'
const navbarCollapsed = 64;
const navbarExpanded = 250;
const initialSelect = 'Все курсы';
class Course extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      categories :null,
      expanded: false,
      selected: initialSelect,
      selected_course: null,
      lesson_list: null,
      selectedIndex:null,
      progress: null,
      completed: false,
      final_test_result: false,
    };
  }


  getCourseProgress = () => {
    return axios.get(REQUESTS.GET_COURSE_RESULTS, {
        params: {
           course: this.props.match.params.courseName
        },
        headers :{
          'user-key':localStorage.getItem('token')
        }
    })
  }


  getLessonList = () => {
    let codename = this.props.match.params.courseName;
    console.log(codename)
    return axios.get(REQUESTS.GET_LESSON_LIST, {
      params: {
        course: codename
      },
      headers: {
        'user-key': localStorage.getItem('token')
      }
    })
  }

  isCourseComplete = () => {
    var {lesson_list, progress} = this.state;
    if (lesson_list === null || progress === null) return;
    for (var i = 0; i<lesson_list.length; i++){
        if (progress[i] == false) return false;
    }
    return true;
  }

  redirectBack = () => {
    this.props.history.push(ROUTES.HOME)
  }

  handleListItemClick = (event, index) => {
    let lesson_id = index;
    let codename = this.props.match.params.courseName;
    console.log("Lesson ID:"+lesson_id);
    console.log('Course name'+codename);
    this.props.history.push('/course/'+codename+'/lesson/'+lesson_id);
  };

  componentDidMount() {
    this.getLessonList().then((res) => {
        console.log(res)
        this.setState({lesson_list:res.data});
        this.getCourseProgress().then(res=>{
          console.log('Progress: ', res);
          this.setState({progress:res.data.user_progress, final_test_result: res.data.user_final_test_result});
          this.setState({ completed:this.isCourseComplete()});
          console.log('Completed:', this.state.completed)
        })
    })
    
  }
  render() {
    let {lesson_list,progress, completed} = this.state
    let codename = this.props.match.params.courseName;
    return(
      <div>
        <Navbar title={'Список лекций'} marginLeft={(this.state.expanded)?navbarExpanded:navbarCollapsed}/>
        <CourseSideNav 
          onSelect={(selected)=>{this.setState({selected:selected})}}
          onToggle={(expanded)=>{this.setState({expanded:expanded})}} 
          categories={(this.state.categories === null)?null:this.state.categories} 
          expanded={this.state.expanded}
          redirectBack={this.redirectBack}
          />
        <div className="courses-list"style={{marginLeft: (this.state.expanded)?navbarExpanded+30:navbarCollapsed+30}}>
        <List 
        style={{width: '650px'}}
        className="list root" 
        style={ {width: '100%'}}
        subheader={<ListSubheader style={{fontSize:'1.5em'}}>Состав курса:</ListSubheader>}>
           {(lesson_list!==null)?lesson_list.map((item,index) => (
                  <>
                      <ListItem 
                      key={item}
                      alignItems="flex-start"
                      button
                      onClick={(event) => this.handleListItemClick(event, lesson_list.indexOf(item))}
                      >
                      <ListItemAvatar>
                        <Avatar>
                          <i className={"fa "+item.icon+""}> </i>
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary={item.title}
                          secondary={
                            <React.Fragment>
                              <Typography
                                component="span"
                                variant="body2"
                                className="list inline"
                                color="textPrimary"
                                style={{display: 'inline'}}
                              >
                                {item.desc}
                              </Typography>
                            </React.Fragment>
                          }
                        />
                        <ListItemIcon>
                          <StarIcon style={{color:(progress !== null && progress[index]===true)?'gold':''}}/>
                        </ListItemIcon>
                      </ListItem>
                      <Divider variant="inset" component="li" />
                 </>
                    )):null}
                    {(completed === true)
                    ? <ListItem
                        alignItems="flex-start"
                        button
                        onClick={()=>{this.props.history.push('/course/'+codename+'/final_test');}}
                        >
                        <ListItemAvatar>
                          <Avatar >
                            <AssignmentLateIcon/>
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary='Финальный тест курса'
                          secondary={
                            <React.Fragment>
                              <Typography
                                component="span"
                                variant="body2"
                                className="list inline"
                                color="textPrimary"
                                style={{display: 'inline'}}
                              >
                                Проверка знаний, полученных в ходе курса 
                              </Typography>
                            </React.Fragment>
                            
                          }
                          
                        /> 
                        <ListItemIcon>
                          {(this.state.final_test_result === true)?<CheckIcon/>:null}
                        </ListItemIcon>
                      </ListItem>
                    : null
                    }
          </List>
        </div>
      </div>
    )
  }
}



class CourseSideNav extends Component {


    render() {
      return (
        <SideNav style={{display:'block', width:this.props.expanded?navbarExpanded:navbarCollapsed}}
          onSelect={this.props.onSelect}
          onToggle={this.props.onToggle}
        >
          <SideNav.Toggle/>
          <SideNav.Nav defaultSelected={initialSelect}>
            <NavItem eventKey={initialSelect}>
                <NavIcon>
                <i className="fa fa-graduation-cap" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                  Все курсы
                </NavText>
            </NavItem>
            {
              (this.props.categories !== null)?
              this.props.categories.map((item)=>(
              <NavItem eventKey={item} key={item}>
                <NavIcon>
                   <i className="fa fa-language" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                  {item}
                </NavText>

              </NavItem>
            )):null}
            <SideBarBack expanded={this.props.expanded} redirectBack={this.props.redirectBack} title={"К списку курсов"}/>
          </SideNav.Nav>
        </SideNav>
      )
    }

}

export default Course