import React, {Component} from "react";
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';
import SingleType from './SingleTypeTest'
import MultiType from './MultiTypeTest'
import InputType from './InputTypeTest';
import Timer from './Timer';
import ResultModal from './ResultModal';
class CourseTest extends Component {

    constructor(props){
        super(props);
        this.state = {
            test: null,
            current_question: 0,
            right_answers: 0,
            user_answers: [], // [Object({answers:Array, is_right:Boolean})]
            show_modal: false,
            test_time: 0,
            time_up: false,
        }
    }

    getFinalTest = () => {
        let codename = this.props.match.params.courseName;
        console.log(codename)
        return axios.get(REQUESTS.GET_COURSE_TEST, {
          headers: {
            'user-key': localStorage.getItem('token')
          },
          params: {
              course: codename,
          }
        })
      }

    componentDidMount() {
        this.getFinalTest().then((res) => {
            console.log(res)
            this.setState({
              test:res.data.test,
              test_time:res.data.test_time,
            });
            
      })
    }

    putFinalTestResult() {
        return axios.put(REQUESTS.PUT_FINAL_RESULT,{}, {
            headers: {
                'user-key': localStorage.getItem('token')
            }, 
            params: {
                course: this.props.match.params.courseName,
            }
        })
    }

    showQuestion = (type) => {
        let {test, current_question} = this.state
        if (type == 'single')
            return <SingleType question={test[current_question]} count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
        else if (type == 'multi')
            return <MultiType question={test[current_question]}  count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
        else if (type == 'input')
            return <InputType question={test[current_question]}  count = {test.length} nextQuestion={this.nextQuestion} num={current_question+1}/> 
    }

    nextQuestion = (answers) => {
        let {current_question} = this.state;
        let result = answers.is_right; 
        let user_answers = this.state.user_answers;
        let new_val = (result)?++this.state.right_answers:this.state.right_answers
        let new_cur_val = current_question
        let show_modal = false;

        user_answers.push(answers);
        if (current_question < this.state.test.length-1) new_cur_val++;
        else show_modal = true;
        this.setState({current_question:new_cur_val, right_answers:new_val, show_modal: show_modal, user_answers:user_answers})
        console.log('right ans: '+ new_val)
        console.log("User Answers: ", user_answers)
    }

    handleClose = () => {
        this.setState({show_modal:false})
        this.handleRedirect();
    }

    handleTimeUp = () => {
        this.setState({time_up:true, show_modal: true})
    }

    handleRedirect = () => {
        var {right_answers,test} = this.state;
        var question_num = test.length;

        console.log("right: ", right_answers, " question num: ", question_num);
        let curr_url = window.location.pathname;
        curr_url = curr_url.split('/');
        curr_url = curr_url.slice(0,curr_url.length-1);
        curr_url = curr_url.join('/');
        if (right_answers/question_num*100 >= 60) this.putFinalTestResult();
        this.props.history.push(curr_url)
    }

    render() {
        let {test, current_question} = this.state
        return (
        <>
            {test !== null ? this.showQuestion(test[current_question].type) : null} 
            
            {(test !== null)?
            <>
            <Timer time={1} time_up={this.handleTimeUp}/>
            <ResultModal 
                open={this.state.show_modal} 
                test={this.state.test} 
                user_answers={this.state.user_answers}
                right_ans={this.state.right_answers}
                pass={this.state.right_answers/test.length*100>=60}
                onClose={this.handleClose}
                time_up= {this.state.time_up}
                />
            </>: null}
        </>)
    }



}

export default withRouter(CourseTest);