import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class LessonTestResultModal extends Component{ 
    render(){
        let {right_ans, question_num, handleClose, handleRedirect, show} = this.props;
        return (<Dialog
        open={show}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        >
        <DialogTitle id="alert-dialog-slide-title">{"Результаты освоения лекции"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
                Вы ответили правильно на {right_ans} из {question_num}.
                {(right_ans/question_num*100 < 60)? 
                "Вы недостаточно хорошо усвоили материал. Прочтите лекции еще раз и попробуйте снова.":
                "Вы успешно освоили материал лекции. Поздравляем!"}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleRedirect} color="primary">
             К списку лекций
          </Button>
        </DialogActions>
      </Dialog>)
    }
}

export default LessonTestResultModal;