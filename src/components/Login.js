import React, { Component, Fragment } from 'react';
import TextInputGroup from './TextInputGroup';
import {withFirebase} from './Firebase/index';
import axios from 'axios';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : '',
            password : ''
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    getToken = (user_id) => {
        axios.get('https://tranquil-fortress-68060.herokuapp.com/api/firebase/getUserToken', {
            params: {
                fb_id: user_id
            }
        }).then((res) => {
            console.log(res);
        }).catch((err)=>{
            console.log(err);
        })
    }

    handleSubmit= (e) => {
        e.preventDefault();
        this.props.firebase.doSignInWithEmailAndPassword(this.state.email,this.state.password)
        .then((user_info)=>{
            console.log(user_info);
            this.getToken(user_info.user.uid);
        }).catch((err)=>{
            console.log(err);
        })
    }

    render() {
        return (
           <Fragment>
               <form onSubmit={this.handleSubmit}>
                   <TextInputGroup 
                   label="E-Mail"
                   id="email"
                   name="email"
                   value={this.state.email}
                   onChange={this.onChange}
                   />
                   <TextInputGroup
                   type="password"
                   label="Password"
                   id="password"
                   name="password"
                   value={this.state.password}
                   onChange={this.onChange}
                   />
                   <button type="submit" value="Login"> Login </button>
               </form>
           </Fragment>
        );
    };
}

export default withFirebase(Login);