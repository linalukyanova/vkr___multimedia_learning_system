import React, { Component, Fragment } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ListSubheader from '@material-ui/core/ListSubheader';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import Navbar from '../Abar';
import styled from 'styled-components';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import axios from 'axios';
import * as REQUESTS from '../../constants/requests';
import { Modal } from 'react-responsive-modal';

const navbarCollapsed = 64;
const navbarExpanded = 250;
const initialSelect = 'Все курсы';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      courses: null,
      categories :null,
      expanded: false,
      selected: initialSelect,
      selected_course: null,
      lesson_list: null,
      open: false,
    };
  }

  openCourseInfo = (codename) => {
    console.log("codename: "+codename)
    axios.get(REQUESTS.GET_LESSON_LIST, {
      params: {
        course: codename
      },
      headers: {
        'user-key': localStorage.getItem('token')
      }
    }).then(res=>{
      console.log(res);
      for (let course of this.state.courses) {
        if (course['codename']===codename){
          this.setState({selected_course:course});
          break;
        }
      }
      this.setState({
        lesson_list:res.data,
        open:true
      })
      
    })
  }

  getCourseList = () => {
    return axios.get(REQUESTS.GET_COURSES, {
        headers :{
        'user-key':localStorage.getItem('token')
      }
    })
  }

  signOnCourse = (codename) => {
    let token = localStorage.getItem('token');
    console.log('Запрошенный курс: '+codename );
    console.log('Токен: '+token );
    axios.post(REQUESTS.PUT_SIGN_ON_COURSE,[], {
      headers: {
        'user-key':token
      },
      params: {
        course: codename
      }
      
    }).then(res =>{
      this.props.history.push('/course/'+codename);
    }).catch(error=>{
      console.log(error)
    })
  }

  renderCoursesCards= () => {
    if (this.state.courses === null) return null;
    if (this.state.selected === initialSelect) {
    return (
        <div style={{display: 'flex'}}>
          {this.state.courses.map(course=>(
            <Card shadow={5} style={{maxWidth: 345, margin: 'auto'}}>
              <CardMedia
                image={REQUESTS.GET_IMAGE+'?image='+course['codename']}
                title="Contemplative Reptile"
                style={{color: '#fff', height: '176px'}}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                {course['title']}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                  across all continents except Antarctica
                </Typography>
              </CardContent>
            <CardActions border> 
            {course['user_progress']!==undefined?
                    <Button colored onClick={()=>{this.props.history.push('/course/'+course['codename'])}}>
                     Продолжить курс
                    </Button>:
                   <Button colored onClick={(e)=>this.signOnCourse(course['codename'])} >Записаться на курс</Button>}
                  <Button colored onClick={(e)=>this.openCourseInfo(course['codename'])}>Просмотреть курс</Button>
                
            </CardActions>
          </Card>
          ))}
        </div>
        )
    }
    else {
      return (
        <div style={{display: 'flex'}}>
          {this.state.courses.map(course=>{
            if (course['category']===this.state.selected) {
             return (<Card shadow={5} style={{maxWidth: 345, margin: 'auto'}}>
              <CardMedia
              image={REQUESTS.GET_IMAGE+'?image='+course['codename']}
              title="Contemplative Reptile"
              style={{color: '#fff', height: '176px'}}
             />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {course['title']}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                across all continents except Antarctica
              </Typography>
            </CardContent>
          <CardActions border> 
          {course['user_progress']!==undefined?
                  <Button colored onClick={()=>{this.props.history.push('/course/'+course['codename'])}}>
                   Продолжить курс
                  </Button>:
                 <Button colored id={course['codename']} onClick={this.signOnCourse}>Записаться на курс</Button>}
                <Button colored onClick={this.openCourseInfo} id={course['codename']}>Просмотреть курс</Button>
              
          </CardActions>
        </Card>)
            }
          })}
        </div>
      );
    }
  }

  onModalClose = () => { this.setState({open:false})}


  componentDidMount() {
    this._asyncRequest = this.getCourseList().then((res) => {
        this.setState({courses:res.data});
        var categorySet = new Set();
        for (let course of res.data) {
          categorySet.add(course['category']);
        }
        this.setState({categories:[...categorySet]});
        console.log(this.state.courses);
        console.log(this.state.categories);
  })
}
  render() {
    return(
      <div>
        <Navbar title={'Все курсы'} marginLeft={(this.state.expanded)?navbarExpanded:navbarCollapsed}/>
        <HomeSideNav 
          onSelect={(selected)=>{this.setState({selected:selected})}}
          onToggle={(expanded)=>{this.setState({expanded:expanded})}} 
          categories={(this.state.categories === null)?null:this.state.categories} 
          expanded={this.state.expanded}
          />
        <div className="courses-grid"style={{marginLeft: (this.state.expanded)?navbarExpanded:navbarCollapsed , marginTop:'70px'}}>
          <h1 style={{textAlign:"center"}}>{this.state.selected}</h1>
          {this.renderCoursesCards()}
        </div>
        <CourseInfoModal open={this.state.open} course={this.state.selected_course} onClose={this.onModalClose} lesson_list={this.state.lesson_list}/>

      </div>
    )
  }
}

 
class CourseInfoModal extends Component {
   
    render() {
      var {course, lesson_list} = this.props;
      return (
        <Modal animationDuration={200}  open={this.props.open} onClose={this.props.onClose} classNames={{modal: 'course-info'}}>
         <Fragment>
          <div class="colmask leftmenu">
            <div class="colleft">
              <div class="col1">
              <h2>{course!==null?course.title:null}</h2>
                    {course!==null?<img src={REQUESTS.GET_IMAGE+'?image='+course['codename']} style={{objectFit:"cover"}} width="128px" height="128px"/>:null}
                    
              </div>

            </div>
            <div class="colright">
            <div class="col2">
            <List 
                  className="list root" 
                  style={ {width: '100%'}}
                  subheader={<ListSubheader style={{width:'90%'}}>Состав курса:</ListSubheader>}
                  >
              {(lesson_list!==null)?lesson_list.map(item => (
                  
                      <ListItem alignItems="flex-start">
                      <ListItemAvatar>
                          <Avatar>
                            У
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary={item.title}
                          secondary={
                            <React.Fragment>
                              <Typography
                                component="span"
                                variant="body2"
                                className="list inline"
                                color="textPrimary"
                                style={{display: 'inline'}}
                              >
                                Информация об уроке
                              </Typography>
                            </React.Fragment>
                          }
                        />
                      </ListItem>
                      // <Divider variant="inset" component="li" />
                   
                )):null}
                </List>
              </div>
            </div>
          </div>
        </Fragment>
        </Modal>
      )
    }
}




class HomeSideNav extends Component {


    render() {
      return (
        <SideNav style={{display:'block', width:this.props.expanded?navbarExpanded:navbarCollapsed}}
          onSelect={this.props.onSelect}
          onToggle={this.props.onToggle}
        >
          <SideNav.Toggle/>
          <SideNav.Nav defaultSelected={initialSelect}>
            <NavItem eventKey={initialSelect}>
                <NavIcon>
                <i className="fa fa-graduation-cap" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                  Все курсы
                </NavText>
            </NavItem>
            {
              (this.props.categories !== null)?
              this.props.categories.map((item)=>(
              <NavItem eventKey={item} key={item}>
                <NavIcon>
                <i className="fa fa-language" style={{ fontSize: '1.75em' }} />
                </NavIcon>
                <NavText>
                  {item}
                </NavText>

              </NavItem>
            )):null}
          </SideNav.Nav>
        </SideNav>
      )
    }

}


export default Home;
