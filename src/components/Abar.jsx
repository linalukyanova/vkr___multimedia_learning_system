import React, {Component} from 'react';
import {withFirebase} from './Firebase/index'
import { withRouter, Redirect } from 'react-router-dom';
import { compose } from 'recompose';
import * as ROUTES from '../constants/routes'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Avatar from '@material-ui/core/Avatar';
import AccountSettingModal from './Profile/Account';




class ABar extends Component  {
    constructor(props){
        super(props);
        this.state = { anchorElem: null, open: false, open_modal: false };
        this.menuRef = React.createRef();
    }

    signout = () => {
        localStorage.removeItem('token');
        this.props.firebase.doSignOut().then((res)=>{
            this.props.history.push(ROUTES.LANDING)
        })
    }


    redirectTo=(pathto)=>(
        this.props.history.push(pathto)
    )

    flipOpen = () => this.setState({ ...this.state, open: !this.state.open });
    changeAnchor = (target) => this.state.anchorElem === null? this.setState({anchorElem:null}):this.setState({anchorElem:target})
    handleClick = event => {
      console.log(event.currentTarget)
      let target = event.currentTarget
      this.state.anchorElem === null? this.setState({anchorElem:null, open: !this.state.open}):this.setState({anchorElem:target,open: !this.state.open})
      this.flipOpen();
    };

    render() {
        const open = this.state.anchorElem === null ? false : true;
        console.log(this.menuRef.current)
        console.log(this.state.anchorElem);
        console.log(this.state.open);
        console.log(open)
        return (
          <>
              <AppBar position="static" 
              style={{
                'margin-left':this.props.marginLeft, 
                'width': ((window.screen.width-this.props.marginLeft)*100/window.screen.width) +'%',
                'background-color':'#db3d44'}}>
                <Toolbar>
                  <Typography variant="h6" className="title" styles={{flexGrow: 1}}>
                  {this.props.title}
                  </Typography>
                      <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                        ref={this.menuRef}
                        color="inherit"
                        style={{marginLeft:'auto'}}
                      >
                        <Avatar src={localStorage.getItem('user_avatar')}></Avatar>
                      </IconButton>
                      <Menu
                        id="menu-appbar"
                        anchorEl={this.menuRef.current}
                        anchorOrigin={{
                          vertical: 'bottom',
                          horizontal: 'left',
                        }}
                        keepMounted
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                        open={this.state.open}
                        onClose={this.flipOpen}
                      >
                        <MenuItem onClick={(e)=>this.redirectTo(ROUTES.PROFILE)}>Профиль</MenuItem>
                        <MenuItem onClick={()=>{this.flipOpen(); this.setState({open_modal:true})}}>Мой аккаунт</MenuItem>
                        <MenuItem onClick={this.signout}>Выход</MenuItem>
                      </Menu>
                </Toolbar>
              </AppBar>
              <AccountSettingModal open={this.state.open_modal} handleClose={() => this.setState({open_modal:false})}/>
              </>
          );
    } 
  }

export default compose(withFirebase, withRouter)(ABar);