import React, { Component, Fragment } from 'react';
import TextInputGroup from '../TextInputGroup';
//import '../../styles/signin.css'
import {withFirebase} from '../Firebase/index';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import axios from 'axios';
import * as ROUTES from '../../constants/routes';

function getToken (user_id, comp) {
  axios.get('https://tranquil-fortress-68060.herokuapp.com/api/firebase/getUserToken', {
      params: {
          fb_id: user_id
      }
  }).then((res) => {
      console.log(res['data']['token']);
      localStorage.setItem('token', res['data']['token']);
      comp.props.history.push(ROUTES.PROFILE);
      
  }).catch((err)=>{
      console.log(err);
  })
}

class SignInFormBase extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : '',
            password : ''
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    

    handleSubmit= (e) => {
        e.preventDefault();
        this.props.firebase.doSignInWithEmailAndPassword(this.state.email,this.state.password)
        .then((user_info)=>{
            console.log("URL: ",user_info.user.photoURL);
            localStorage.setItem('username', user_info.user.displayName);
            localStorage.setItem('user_avatar',user_info.user.photoURL);
            getToken(user_info.user.uid, this);
        }).catch((err)=>{
            console.log(err);
        })
    }

    render() {
        return (
           <Fragment>
                <div className="wrapper">
                    <div className={'authContainer'}>
                        <div className={'leftBox'}>
                            <div className={'bgGreen'}/>
                            <div className={'imageAuth'}/>
                            <div className={'imageText'}>Начни учиться прямо сейчас!</div>
                        </div>
                        <div className={'rightBox'}>
                            <form onSubmit={this.handleSubmit} className={'form'}>
                                <div className={'titleAuth'}>Авторизация</div>
                                <div className={'inputSBox'}>
                                    <TextInputGroup 
                                     id="email"
                                     name="email"
                                     value={this.state.email}
                                     onChange={this.onChange}
                                     className={'inputS'} 
                                     placeholder={'Email'}
                                     />
                                </div>
                                <div className={'inputSBox'}>
                                     <TextInputGroup
                                        className={'inputS'}
                                        type="password"
                                        id="password"
                                        name="password"
                                        placeholder={'Password'}
                                        value={this.state.password}
                                        onChange={this.onChange}
                                     />
                                </div>    
                                <div className={'contentBox'}>
                                    <div className={'checkBoxContainer'}>
                                        <input type={'checkBox'} className={'checkbox'} />
                                        <label className={'checkBoxLabel'}>Запомнить меня</label>
                                    </div>
                                    <div className={'text1'} >Забыли пароль?</div>
                                </div>
                                <button type="submit" value="Login" className={'btnAuth'}> Login </button>
                            
                                <div className={'borderBox'}>
                                    <div className={'line'} />
                                    <div className={'text2 or'}>OR</div>
                                </div>
                                <div className={'socialMediaBox'}>
                                    <SignInGoogle />
                                    <SignInFacebook />
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>   
           </Fragment>
        );
    };
}


class SignInGoogleBase extends Component {
    constructor(props) {
      super(props);
  
      this.state = { error: null };
    }
  
    onSubmit = event => {
      this.props.firebase
        .doSignInWithGoogle()
        .then(socialAuthUser => {
          console.log("we're in")
          console.log(socialAuthUser);
          localStorage.setItem('username', socialAuthUser.user.displayName);
          localStorage.setItem('user_avatar',socialAuthUser.user.photoURL);
          getToken(socialAuthUser.user.uid, this);
        })
        .catch(error => {
          this.setState({ error });
        });
  
      event.preventDefault();
    };
  
    render() {
      const { error } = this.state;
  
      return (
        <div>
          <button type="button" className={'icAuth google'} onClick={this.onSubmit} ></button>
  
          {error && <p>{error.message}</p>}
          </div>
      );
    }
  }
  
  class SignInFacebookBase extends Component {
    constructor(props) {
      super(props);
  
      this.state = { error: null };
    }
  
    onSubmit = event => {
      this.props.firebase
        .doSignInWithFacebook()
        .then(socialAuthUser => {
          // Create a user in your Firebase Realtime Database too
          localStorage.setItem('username', socialAuthUser.user.displayName);
          localStorage.setItem('user_avatar',socialAuthUser.user.photoURL);
          getToken(socialAuthUser.user.uid, this);
        })
        .catch(error => {
          this.setState({ error });
        });
  
      event.preventDefault();
    };
  
    render() {
      const { error } = this.state;
  
      return (
        <div onSubmit={this.onSubmit}>
          <button type="button" onClick={this.onSubmit}className="icAuth facebook"></button>
          {error && <p>{error.message}</p>}
        </div>
      );
    }
  }
  const SignInForm = compose(
    withRouter,
    withFirebase,
  )(SignInFormBase);
  
  const SignInGoogle = compose(
    withRouter,
    withFirebase,
  )(SignInGoogleBase);
  
  const SignInFacebook = compose(
    withRouter,
    withFirebase,
  )(SignInFacebookBase);

  const SignInPage = SignInForm;
  
  export default SignInPage;